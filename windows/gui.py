import tkinter
import os
from os import system as runcmd
from pathlib import Path

root = tkinter.Tk()

def main():
    cwd = Path().absolute()
    startUpCommand = ("py " + str(cwd) + "/main.py")
    print(startUpCommand)
    startUpCommand = str(startUpCommand)
    runcmd(startUpCommand)

B = tkinter.Button(root, text ="Diktálás", command = main)

B.pack()

root.title('ROBIN')
root.mainloop()
