from gtts import gTTS
from playsound import playsound

# This the os module so we can play the MP3 file generated
import os

# Generate the audio using the gTTS engine. We are passing the message and the language
audio = gTTS(text='Köszöntelek, ROBIN vagyok!', lang='hu')

# Save the audio in MP3 format
audio.save("currentMessage.mp3")

playsound('currentMessage.mp3')
# Play the MP3 file
