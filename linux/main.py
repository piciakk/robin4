print("ROBIN v4.0")
from os import getcwd as currentWorkingDirectory
from os import system as runcmd
from os import popen as runcmdWithOutput
try:
    from gtts import gTTS
    from playsound import playsound
    import speech_recognition as sr
    from googletrans import Translator
    import broadlink
    import webbrowser
    import wikipedia
    import ntpath
    import qrcode
    from twilio.rest import Client
    import pyowm
    from kodijson import *
    from samsungtv import SamsungTV
except ImportError as e:
    runcmd("sudo apt install libasound2-dev")
    runcmd("sudo sudo apt install qmmp")
    runcmd("python3 -m pip install playsound")
    runcmd("python3 -m pip install gtts")
    runcmd("sudo apt-get install espeak")
    runcmd("conda install portaudio")
    runcmd("conda install pyaudio")
    runcmd("python3 -m pip install googletrans")
    runcmd("python3 -m pip install SpeechRecognition")
    runcmd("python3 -m pip install PyAudio")
    runcmd("python3 -m pip install broadlink")
    runcmd("python3 -m pip install wikipedia")
    runcmd("python3 -m pip install requests")
    runcmd("python3 -m pip install qrcode[pil]")
    runcmd("python3 -m pip install twilio>=6.0.0")
    runcmd("python3 -m pip install pyowm")
    runcmd("python3 -m pip install kodi-json")
    runcmd("python3 -m pip install samsungtv")
def say(textToSay):
    print(textToSay)
    audio = gTTS(text=textToSay, lang='hu')
    audio.save("currentMessage.mp3")
    playsound('currentMessage.mp3')

def main():
    usernameFile = open(currentWorkingDirectory() + "/username", "r+")

    lastQuestionFile = open(currentWorkingDirectory() + "/lastQuestion", "r+")

    contactsFile = open(currentWorkingDirectory() + "/contacts", "r+")
    contacts = contactsFile.readlines()
    numberOfContacts = len(contacts)

    settingsFile = open(currentWorkingDirectory() + "/settings", "r+")
    settings = settingsFile.readlines()

    KodiIP = settings[4].split()
    KodiIP = KodiIP[2]

    KodiPORT = settings[5].split()
    KodiPORT = KodiPORT[2]

    KodiUSERNAME = settings[6].split()
    KodiUSERNAME = KodiUSERNAME[2]

    KodiPASSWORD = settings[7].split()
    KodiPASSWORD = KodiPASSWORD[2]

    WeatherUnit = settings[2].split()
    WeatherUnit = WeatherUnit[2]

    OWMAPIKEY = settings[3]
    OWMAPIKEY = OWMAPIKEY[2]

    TwillioAccountSID = settings[0].split()
    TwillioAccountSID = TwillioAccountSID[2]
    TwillioAuthToken = settings[1].split()
    TwillioAuthToken = TwillioAuthToken[2]

    tvIPAddress = settings[8].split()
    tvIPAddress = tvIPAddress[2]

    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Hallgatlak...")
        audio_text = r.listen(source)
        try:
            print(r.recognize_google(audio_text, language="hu-HU"))
            question = r.recognize_google(audio_text, language="hu-HU")
        except:
            print("Sajnálom, nem sikerült felismernem, amit mondtál.")
    owm = pyowm.OWM("5eff1be66539b6210425df457ef73892")
    translator = Translator()
    englishQuestion = translator.translate(question, dest='en', src='hu')

    lastQuestion = lastQuestionFile.read()
    lastQuestion = lastQuestion.rstrip('\x00')

    if "szia" in question.lower() or "hi" in question.lower() or "helló" in question.lower() or "hello" in question.lower() or "cső" in question.lower() or "csá" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        say("Szia, " + usernameFile.readline())
    elif "hogy hívnak" in question.lower() or "mi a neved" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        if usernameFile.read() == "":
            answer = "Az én nevem ROBIN! És a tiéd?"
        else:
            answer = "Az én nevem ROBIN!"
        say(answer)
    elif "hogy hívnak" in lastQuestion or "mi a neved" in lastQuestion:
        usernameFile.write(question)
    elif "kapcsold fel a lámpát" in question.lower() or "kapcsold be a lámpát" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        try:
            devices = broadlink.discover(timeout=10)
            devices[0].auth()
            devices[0].set_power(True)
            answer = "Lámpa sikeresen felkapcsolva!"
        except:
            answer = "Sikertelen felkapcsolás!"
        say(answer)
    elif "kapcsold le a lámpát" in question.lower() or "kapcsold ki a lámpát" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        try:
            devices = broadlink.discover(timeout=10)
            devices[0].auth()
            devices[0].set_power(False)
            answer = "Lámpa sikeresen lekapcsolva!"
        except:
            answer = "Sikertelen lekapcsolás!"
        say(answer)
    elif "mozi mód" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        try:
            devices = broadlink.discover(timeout=10)
            devices[0].auth()
            devices[0].set_power(False)
            webbrowser.open('https://hbogo.hu/filmek')
            answer = "Mozi mód bekapcsolása sikeres. Kérlek válaszd ki azt a filmet, amit meg szeretnél nézni!"
        except:
            answer = "Mozi mód bekapcsolása sikertelen"
        say(answer)
    elif "fájl" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        say("Kérlek, húzd ide a fájlt, vagy manuálisan írd be az elérési útvonalát!")
        try:
            filePath = input()
            fileName = ntpath.basename(filePath)
            targetURL = runcmdWithOutput("curl --upload-file " + filePath + " https://transfer.sh/" + fileName).read()
            img = qrcode.make(targetURL)
            img.show()
            say("Sikeres fájlfeltöltés! Szkenneld be a QR kódot, vagy küld át valakinek!")
        except:
            say("Sikertelen fájlfeltöltés! Próbáld újra!")
    elif "némítás" in question.lower() or "néma" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        runcmd("osascript -e 'set Volume 0'")
    elif "hangerő" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        say("Hányasra állítsam be a hangerőt?")
    elif "hangerő" in lastQuestion:
        runcmd("osascript -e 'set Volume " + question + "'")
    elif "üzenet" in question.lower() or "írj" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())

        client = Client(TwillioAccountSID, TwillioAuthToken)
        target = question.split()
        target = target[len(target)-1]
        target = list(target)

        target[len(target)-1] = ""
        target[len(target)-2] = ""
        target[len(target)-3] = ""
        target = ''.join(target)

        target = list(target)

        if target[len(target)-1] == "á":
            i = 0
            while i <= numberOfContacts:
                currentContact = contacts[i-1]
                currentContact = currentContact.split()
                print(currentContact[0])
                if currentContact[0] == target:
                    modify = False
                    break
                else:
                    modify = True
                i += 1
            target = list(target)
            if modify == True:
                target[len(target)-1] = "a"
            target = ''.join(target)
            print(target)
        elif target[len(target)-1] == "é":
            i = 0
            while i <= numberOfContacts:
                currentContact = contacts[i-1]
                currentContact = currentContact.split()
                print(currentContact[0])
                if currentContact[0] == target.lower():
                    modify = False
                    break
                else:
                    modify = True
                i += 1
            if modify == True:
                target[len(target)-1] = "e"
            target = ''.join(target)
            print(target)
            #say("Sajnálom, de nem sikerült bejelentkeznem a Twillio fiókodba! Frissítsd a bejelentkezési adatokat a settings fájlban.")
    elif "időjárás" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        weather = owm.weather_at_place('Budapest, HU').get_weather()
        answer = ("A jelenlegi hőmérséklet " + str(weather.get_temperature(WeatherUnit)['temp']) + " Celsius fok. A")
        say(answer)
    elif "útvonalterv" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
        say("Áruldd el, hogy hova tervezzem az útvonaladat!")
    elif "útvonalterv" in lastQuestion:
        place = question.replace(' ', '+')
        webbrowser.open("https://www.google.com/maps/dir//" + place)
        say("Sikeres útvonaltervezés!")
    elif "nyisd meg" in question.lower():
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question.lower())
    else:
        lastQuestionFile.truncate(0)
        lastQuestionFile.write(question)
        wikipedia.set_lang("hu")
        answer = wikipedia.summary(question)
        print(answer)
        engine.say(answer)
        engine.runAndWait()
    contactsFile.close()
    settingsFile.close()
    usernameFile.close()
    lastQuestionFile.close()
    closeOrNot = input()
    if closeOrNot != "":
        exit()
    else:
        main()
main()
