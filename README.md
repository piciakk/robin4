# ROBIN4 #

ROBIN egy hangvezérelt rendszer, sok integrációval és nagy tudástárral.

## Funkciók ##

### Szeretjük az IoT-t ###

ROBIN sok IoT (Internet Of Things, Dolgok Internetje) eszközzel együttműködik, tud okos lámpát, okos konnektort kapcsolgatni, stb..
* [Broadlink eszközök támogatása python-broadlink module-al](https://github.com/mjg59/python-broadlink)

### Cross-platform mindenek felett ###

* ROBIN már most is cross-platform alkalmazásnak számít, de az elérhető operációs-rendszerek száma folyamatosan növekszik
* OSX: GUI, CLI változat is elérhető
* Windows: GUI változat
* Android: GUI változat, néhány parancs végrehajtása Termux segítségével
* Raspbian: CLI változat
* Ubuntu: CLI változat

### Open-Source ###

* ROBIN kódja teljes egészében Open-Source, és a benne felhasznált modulok, eszközök is.
* Bárki, aki elég tudással rendelkezik, szerkesztheti ROBIN kódját

### Wikipedia integráció ###

* ROBIN bármilyen magyar, vagy angol szócikk tartalmát fel tudja olvasni.
* Abban az esetben, ha nem lenne találat magyar nyelven, angol találatokat néz és automatikusan lefordítja.
* [A Wikipedia támogatás egy pythonos pluginnal történik](https://pypi.org/project/wikipedia/)

### Személyes időjárásjelentés ###

* ROBIN [OpenWeatherMap](https://openweathermap.org/) segítségével tudja az időjárást is
* Geolokáció alapján mindig a helyi időjárást mondja

### WolframAlpha a barátunk ###

* A WolframAlpha óriási tudástárából is merít ROBIN
* Bonyolult matematikai egyenletek megoldása
* Folyamatosan naprakész árfolyamok
* Történelmi tudás

## Hogyan használd ROBIN-t ##

### MacOS ###
* Első lépés: - Kattints [ide a GUI](https://robin.veddvelem.hu/download/macgui.zip), vagy [ide a CLI](https://robin.veddvelem.hu/download/mac.zip) verzió letöltéséhez.
* Második lépés: - Unzippeld a letöltött zip fájlt.
* Harmadik lépés: - Ellenőrizd le, hogy feltelepítetted-e a pythont a rendszeredre a **whereis python** parancssal egy terminal ablakban. Ha az output **/usr/bin/python**, akkor a python telepítve van. Ha az output viszont üres, akkor letöltheted a pythont a [Homebrew Package Manager](https://brew.sh) segítségével: **brew install python**
* Negyedik lépés:

  * a. Folytasd ezzel az al-lépéssel, hogyha ROBIN portable változatát szeretnéd használni.

  * b. Folytasd ezzel az al-lépéssel, hogyha ROBINt fel szeretnéd telepíteni a rendszeredre. Futtasd le az install.py fájlt, ami automatikusan letölti és feltelepíti a szükséges package-eket. Ezek után, ha el szeretnéd indítani ROBIN-t, akkor csak futtasd egy terminal ablakban a **robin**, vagy a **robingui** parancsot.


### Discord ###
* Első lépés: - Töltsd le a rendszeredre a discordot a legjobb élmény elérése érdekében. [Katt ide a letöltéshez](https://discord.com/download).
* Második lépés: -
